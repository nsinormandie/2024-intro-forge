
from pyodide_mkdocs_theme.pyodide_macros import (
    PyodideMacrosPlugin,
    Msg, MsgPlural, TestsToken, Tip,
)


def define_env(env:PyodideMacrosPlugin):
    """ The customization has to be done at macro definition time.
        You could paste the code inside this function into your own main.py (or the
        equivalent package if you use a package instead of a single file). If you don't
        use personal macros so far, copy the full code into a `main.py` file at the root
        of your project (note: NOT in the docs_dir!).

        NOTE: you can also completely remove this file if you don't want to use personal
              macros or customize the messages in the built documentation.

        * Change whatever string you want.
        * Remove the entries you don't want to modify
        * Do not change the keyboard shortcuts for the Tip objects: the values are for
          informational purpose only.
        * See the documentation for more details about which string is used for what
          purpose, and any constraints on the arguments:
          https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/custom/messages/

        ---

        The signatures for the various objects below are the following:

        ```python
        Msg(msg:str)
        MsgPlural(msg:str, plural:str="")
        Tip(width_in_em:int, msg:str, kbd:str=None)
        TestsToken(token_str:str)
        ```
    """

    custom = { # contient uniquement ce qui a été modifié
    # Terminals
        "run_script":    Msg("Script lancé."),
        "install_start": Msg("Installation de paquets python en cours."),
        "install_done":  Msg("Installations terminées."),
        "success_msg":   Msg("Terminé sans erreur."),


    # Terminals: validation success/failure messages
        "success_head":  Msg("Bravo !"),
        "success_tail":  Msg("Vous pouvez lire"),
        "success_head_extra":  Msg("Tous les tests sont passés."),
        "fail_tail":     MsgPlural("est maintenant disponible", "sont maintenant disponibles"),


    # Corr  rems admonition:
        "corr":       Msg('Proposition de correction'),




    # QCMS
        "qcm_mask_tip":  Tip(15, "Les réponses resteront cachées.")
    }

    env.lang.overload(custom)

