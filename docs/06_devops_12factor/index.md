---
title: DevOps
---

# DevOps, CI/CD, etc..

## DevOps

### Présentation

-   DevOps = contraction de "Development" et "Operations"
-   mouvement culturel, philosophique, technique :)
-   objectif : rapprocher les équipes de développement et d'opérations
-   outils : git, intégration continue, déploiement continu, etc..
-   principes : automatisation, collaboration, transparence, etc..

## CI/CD

### CI

-   CI = Continuous Integration
    -   intégration continue
    -   chaque développeur publie régulièrement son code
    -   chaque publication déclenche des tests automatiques
    -   objectif : détecter les problèmes rapidement

### CD

-   CD = Continuous Deployment
    -   déploiement continu
    -   chaque publication est déployée automatiquement
    -   objectif : déployer rapidement et souvent

## Outils

### Liste

-   gitlab, github, bitbucket, etc..
-   jenkins, travis, circleci, etc..
-   docker, kubernetes, etc..

## 12-factor app

### Présentation

-   https://12factor.net/
-   méthodologie pour développer des applications modernes
-   objectif : applications robustes, évolutives, maintenables, etc..
-   12 principes : codebase, dépendances, config, backing services, build-release-run, process, port binding, concurrency, disposabilité, dev/prod parity, logs, admin processes

## Services "intégrés"

### Les principaux

-   gitlab : https://gitlab.com/
-   github : https://github.com/
-   bitbucket : https://bitbucket.org/

différences : https://about.gitlab.com/devops-tools/github-vs-gitlab.html (gitlab plus complet et partie opensource, github plus connu mais moins complet, propriétaire et Microsoft)

### Services proposés par git[lab|hub] :

-   gestion de version (git) : code source, branches, commits, etc.. avec interface web
-   issues / tickets / bug tracking : gestion des problèmes, des demandes, etc..
-   CI/CD
