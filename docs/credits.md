---
author: PCD NSI Normandie
title: Crédits
---

Le site est hébergé par [la forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/){:target="_blank" }.

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/){target="_blank"} et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/){target="_blank"}. 

Le thème intégrant Pyodide [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/){target="_blank"} est dû à Frédéric Zinelli.

Le modèle du site et les tutoriels qui les accompagnent ont été créés par l'[Association des enseignantes et enseignants d'informatique de France](https://aeif.fr/index.php/category/non-classe/){target="_blank"} et en particuler par Vincent-Xavier Jumel,  Mireille Coilhac et Charles Poulmaire, que nous remercions vivement.

Enfin, le contenu de ce site a été élaboré par Jean-Matthieu Barbier, Nathalie Maïer et Nathalie Weibel, enseignants de NSI.