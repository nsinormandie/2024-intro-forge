# Introduction

Dans cette section, nous allons voir comment utiliser Git pour gérer un projet de site web, seul ou en équipe. Pour cela :

-   Nous ferons une brève présentation de ce qu'est un [système de gestion de version](./01_histoire_git/index.md), ainsi qu'un bref [historique de Git](./01_histoire_git/index.md#histoire-de-git),
-   Nous allons d'abord voir [comment installer Git sur votre machine](./02_installation/index.md)
-   Nous allons ensuite voir [comment configurer Git pour votre projet](./03_utilisation/index.md)
-   Nous allons voir comment [utiliser git pour gérer votre projet](./03_utilisation/index.md)
-   Interlude python : nous verrons comment [travailler en local](./04_travailler_local/index.md) sur votre site.
-   Enfin nous jetterons un coup d'oeil sous le capot pour voir [comment fonctionne Git](./05_fonctionnement/index.md)
