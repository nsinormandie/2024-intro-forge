# Installation de git sous Windows via Chocolatey

## Installation chocolatey

<figure>
<img src="../screenshots/win-choco/01-launch-powershell-as-admin.png" class="img-fluid w-full">
<figcaption class="text-sm">Lancer PowerShell en tant qu'administrateur</figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/02-launch-powershell-as-admin.png" class="img-fluid w-full">
<figcaption class="text-sm">Lancer PowerShell en tant qu'administrateur</figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/03-launch-powershell-as-admin.png" class="img-fluid w-full">
<figcaption class="text-sm">Lancer PowerShell en tant qu'administrateur</figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/04-choco-install-script.png" class="img-fluid w-full">
<figcaption class="text-sm">Installer chocolatey comme indiqué sur le site de chocolatey</figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/05-choco-install-script.png" class="img-fluid w-full">
<figcaption class="text-sm">Installer chocolatey comme indiqué sur le site de chocolatey : vérifier le <code>Get-ExecutionPolicy</code></figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/06-choco-install-script.png" class="img-fluid w-full">
<figcaption class="text-sm">Installer chocolatey comme indiqué sur le site de chocolatey : vérifier le <code>Get-ExecutionPolicy</code></figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/07-choco-install-script.png" class="img-fluid w-full">
<figcaption class="text-sm">Installer chocolatey comme indiqué sur le site de chocolatey : vérifier le<code>Get-ExecutionPolicy</code></figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/09-choco-install-script.png" class="img-fluid w-full">
<figcaption class="text-sm">Installer chocolatey comme indiqué sur le site de chocolatey : modifier le<code>Get-ExecutionPolicy</code></figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/10-choco-install-script.png" class="img-fluid w-full">
<figcaption class="text-sm">Installer chocolatey comme indiqué sur le site de chocolatey : modifier le<code>Get-ExecutionPolicy</code></figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/11-choco-install-script.png" class="img-fluid w-full">
<figcaption class="text-sm">Installer chocolatey comme indiqué sur le site de chocolatey : vérifier le<code>Get-ExecutionPolicy</code></figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/12-choco-install.png" class="img-fluid w-full">
<figcaption class="text-sm">Installer chocolatey comme indiqué sur le site de chocolatey : copier le script d'installation</figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/13-choco-install.png" class="img-fluid w-full">
<figcaption class="text-sm">Installer chocolatey comme indiqué sur le site de chocolatey : coller le script d'installation</figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/14-choco-install.png" class="img-fluid w-full">
<figcaption class="text-sm">Installer chocolatey comme indiqué sur le site de chocolatey : lancer le script d'installation</figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/15-choco-test.png" class="img-fluid w-full">
<figcaption class="text-sm">Réouvrir un shell administrateur, et tester la commande <code>choco</code></figcaption>
</figure>

## Installalation de git via chocolatey

<figure>
<img src="../screenshots/win-choco/16-choco-install-git.png" class="img-fluid w-full">
<figcaption class="text-sm">Lancer l'installation de git :  <code>choco install git</code></figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/17-choco-install-git.png" class="img-fluid w-full">
<figcaption class="text-sm">Lancer l'installation de git :  <code>choco install git</code> - confirmation</figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/18-choco-install-git.png" class="img-fluid w-full">
<figcaption class="text-sm">Lancer l'installation de git :  <code>choco install git</code> - fini</figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/19-check-git-install.png" class="img-fluid w-full">
<figcaption class="text-sm">Vérifier si tout est bien installé</figcaption>
</figure>

<figure>
<img src="../screenshots/win-choco/20-check-git-install.png" class="img-fluid w-full">
<figcaption class="text-sm">Vérifier que la commande `git` est disponible</figcaption>
</figure>
