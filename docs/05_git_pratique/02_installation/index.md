# Installation de git

## Vérification de l'installation

Avant de chercher à installer git, il est possible de vérifier si il est déjà installé sur votre machine.

-   sous linux : ouvrir un terminal, taper `git --version`
-   sous macos : ouvrir l'application "Terminal", taper `git --version`
-   sous windows : rechercher le programme `Git Bash`, si présent l'ouvrir et taper `git --version`

Si une version s'affiche, git est déjà installé sur votre machine. Sinon, si un message d'erreur s'affiche ou que vous ne trouvez pas le programme, il faut l'installer.

## Installation de git

### Sous linux

Si vous utilisez une distribution dérivée de debian (debian, ubuntu, mint, rasbpian, etc..) :

```bash
sudo apt install git
```

Si vous utilisez une distribution dérivée de fedora (fedora, centos, redhat, etc..) :

```bash
sudo dnf install git
```

### Sous macos

Si vous avez installé `XCode`, git est déjà installé. Sinon, vous aussi pouvez l'installer avec [brew](https://brew.sh/fr/) :

```bash
brew install git
```

Si vous n'avez pas installé `brew`, vous pouvez le faire en suivant les instructions sur le site [brew.sh/fr/](https://brew.sh/fr/) :

<div style="font-size: 0.6rem">
```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
</div>

ou en utilisant l'installateur `pkg` à [télécharger sur github](https://github.com/Homebrew/brew/releases/latest) et à installer manuellement.

### Sous windows

L'installation de git sous windows est couplée à l'installation de `Git Bash`, un terminal qui permet d'utiliser git en ligne de commande.

Pour l'installation :

-   `choco install git` (avec [chocolatey](https://chocolatey.org/install)[^1]) - [étapes détaillées](./install-win-choco/index.md).
-   `winget install --id Git.Git -e --source winget` (avec [winget](https://learn.microsoft.com/fr-fr/windows/package-manager/winget/))[^2]
-   ou [télécharger sur git-scm.com](https://git-scm.com/download/win) et installer manuellement le logiciel (utiliser la **version portable** si vous n'avez pas les droits administrateur sur votre machine).

[^1]: [chocolatey](https://chocolatey.org/install) est un gestionnaire de paquets pour windows, qui permet d'installer des logiciels en ligne de commande. Si vous ne le connaissez pas, prenez le temps de le découvrir, il peut vous faciliter la vie.
[^2]: [winget](https://learn.microsoft.com/fr-fr/windows/package-manager/winget/) est un gestionnaire de paquets pour windows, développé par Microsoft. Il est plus récent, un peu moins complet, et n'est pas disponible sur toutes les versions de windows.
