# Développement en local

## Environnement virtuel

De manière générale, lorsqu'un projet python nécessite des modules qui ne font pas partie de la librairie standard (et qui donc doivent être installés localement), il est recommandé de créer un [**environnement virtuel**](https://docs.python.org/3/library/venv.html#module-venv) pour ce projet. Cela permet de ne pas polluer l'installation globale de python, et de ne pas avoir de conflits entre les différentes versions de modules utilisées par les différents projets.

Dans un environnement virtuel, le chemin d'accès aux modules installés est modifié pour pointer vers le répertoire de l'environnement virtuel, et non vers l'installation globale de python. Cela permet d'installer des modules sans les droits administrateurs, et de ne pas avoir de conflits entre les différentes versions de modules utilisées par les différents projets.

Pour créer un environnement virtuel, il suffit de se placer dans le répertoire du projet et de lancer la commande suivante :

```bash
python -m venv .venv
# ou python3 -m venv .venv
# si python ne pointe pas vers python3
```

Cette commande exécute le module `venv` de python, qui crée un environnement virtuel dans le répertoire donné en argument (ici `.venv`). Traditionnellement, le répertoire de l'environnement virtuel est nommé `.venv` ou `venv`, mais il est tout à fait possible de choisir un autre nom.

???info "Pour aller plus loin"

    Comment fonctionne un environnement virtuel ? La documentation est disponible sur [docs.python.org](https://docs.python.org/3/library/venv.html#how-venvs-work). En résumé, le répertoire dans lequel est créé l'environnement virtuel reproduit la forme d'une installation de python (via des liens symboliques). Il suffit donc d'appeler le script python de `bin/python`, et le python "va croire"[^1] qu'il est installé à cet endroit et donc être configuré pour aller chercher les modules en accord avec `sys.prefix` qui pointe sur le répertoire de l'environnement virtuel.

[^1]: Le python n'est pas si naïf, la variable `sys.base_prefix` contient le chemin de la "vraie" installation de python.

Pour utiliser cet environnement virtuel, il suffit d'utiliser l'exécutable `python` qui est situé dans le répertoire `bin` de cet environnement.

```bash
# Exemple sous Linux ou MacOS
.venv/bin/python -c "import sys; print(sys.prefix)"
```

Pour plus de simplicité, il est possible d'activer l'environnement virtuel, ce qui permet d'utiliser directement la commande `python` pour exécuter les scripts python, et d'utiliser les commandes `pip` pour installer des modules, sans avoir à spécifier le chemin complet de l'exécutable python. Pour cela, il faut exécuter le script d'activation correspondant à votre système d'exploitation et à votre shell.

```bash
# Sous Windows avec cmd (non testé)
.venv\Scripts\activate
# Avec un shell de type bash, sous Linux
# ou MacOS, ou sous Windows avec Git Bash
source .venv/bin/activate
# Avec fish
source .venv/bin/activate.fish
```

Lorsque l'environnement virtuel est activé, le nom de l'environnement virtuel apparaît dans le prompt du terminal. Par exemple, si l'environnement virtuel est nommé `.venv`, le prompt ressemblera à ceci :

```bash
(.venv) $
```

Pour désactiver l'environnement virtuel, il suffit de taper la commande `deactivate`.

## Gestion des dépendances

De manière générale, il est important de lister les dépendances d'un projet python, afin qu'un autre utilisateur puisse installer ces dépendances facilement et se retrouver dans le même environnement de développement que le développeur initial. Plusieurs solutions existent pour gérer les dépendances d'un projet python.

### Fichier `requirements.txt`

C'est le plus courant et le plus ancien des formats : le fichier `requirements.txt`. Ce fichier contient la liste des modules nécessaires pour faire fonctionner le projet, avec éventuellement leur version, un par ligne. Par exemple :

```txt
requests==2.25.1
beautifulsoup4==4.9.3
```

Pour installer les dépendances d'un projet, il suffit de se placer dans le répertoire contenant ce fichier `requirements.txt` et de lancer la commande suivante :

```bash
pip install -r requirements.txt
```

Cette commande installe les modules listés dans le fichier `requirements.txt` et toutes leurs dépendances. La résolution des dépendances est faite par `pip`, qui va chercher la version la plus récente des modules listés; selon la date d'installation, il est donc possible que les versions installées ne soient pas exactement les mêmes d'un environnement à l'autre. La commande `pip freeze` permet de lister **tous** les modules installés dans l'environnement virtuel, avec leur version exacte. Pour écrire ces informations dans un fichier `requirements.txt`, il suffit de rediriger la sortie de la commande `pip freeze` vers un fichier :

```bash
pip freeze > requirements.txt
```

Cette solution est relativement simple, mais manque de flexibilité...

### Autres possibilités : `Pipfile` et `pyproject.toml`

La gestion des dépendances en python est un sujet qui est encore en train d'évoluer, et où plusieurs solutions coexistent plus ou moins pacifiquement.

-   `Pipfile` et `Pipfile.lock` : ces fichiers sont utilisés par `pipenv`, un outil qui permet de gérer les dépendances d'un projet python. `Pipfile` contient la liste des dépendances, et `Pipfile.lock` contient la liste des dépendances avec leur version exacte. Pour installer les dépendances d'un projet, il suffit de se placer dans le répertoire contenant ces fichiers et de lancer la commande `pipenv install`.
-   `pyproject.toml` : ce fichier est utilisé par `poetry`, un outil qui permet de gérer les dépendances d'un projet python. Il contient la liste des dépendances, avec leur version exacte. Pour installer les dépendances d'un projet, il suffit de se placer dans le répertoire contenant ce fichier et de lancer la commande `poetry install`. `pyproject.toml` peut aussi être utilisé par `pip` et `pip-tools` pour gérer les dépendances d'un projet.
-   [astral-sh/uv](https://github.com/astral-sh/uv), en cours de développement est un outil à surveiller.

L'utilisation d'un fichier `pyproject.toml`, spécifié [ici](https://packaging.python.org/en/latest/specifications/pyproject-toml/) semble être la solution "officielle" vers laquelle se dirige la communauté python, mais les outils permettant de gérer ce fichier sont encore en évolution.

## Application pour un site mkdocs

MkDocs est livré avec un serveur de développement intégré qui permet de prévisualiser un projet pendant que vous travaillez dessus.

### La première fois

Dans le répertoire du site cloné, **juste après le clonage** :

-   Créer l'environnement virtuel :

```bash
python -m venv .venv
```

-   Activer l'environnement virtuel :

```bash
source .venv/bin/activate
```

-   Installer les dépendances :

```bash
pip install -r requirements.txt
```

-   Lancer le serveur de développement :

```bash
mkdocs serve
# ou bien python3 -m mkdocs serve
```

-   Dans un navigateur, en utilisant l'adresse `http://localhost:8000/`, on peut voir le projet et les modifications apportées au fur et à mesure.
-   ++ctrl+c++ pour fermer le serveur.

-   Lorsqu'on a fini les modifications, on peut ensuite utiliser les commandes `git` suivantes pour modifier le dépôt :

```bash
git status
git add .
git commit -m "message"
git push
```

### Les fois suivantes

A chaque fois qu'on veut travailler sur le projet :

-   Récupérer les modifications éventuelles

```bash
git pull
```

-   Activer l'environnement virtuel

```bash
source .venv/bin/activate
```

-   Si les dépendances ont changé, les installer

```bash
pip install -r requirements.txt
```

-   Lancer le serveur

```bash
mkdocs serve
# ou bien python3 -m mkdocs serve
```

-   On peut ensuite utiliser les commandes suivantes pour modifier le dépôt :

```bash
git status
git add .
git commit -m "message"
git push
```
