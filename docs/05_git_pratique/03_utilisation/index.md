---
title: Introduction
---

# Utiliser git

## Introduction

Pour bien utiliser git, il est préférable de comprendre comment il fonctionne. Pour cela, nous vous conseillons de lire la section [fonctionnement de git](../05_fonctionnement/index.md). Mais pour commencer, nous allons voir comment utiliser git pour gérer le projet de site web que vous avez créé plus tôt.

Git est un outil de gestion de versions **décentralisé**, ce qui signifie que chaque utilisateur possède une copie complète de l'historique du projet. Concrètement, pour l'instant, le seul exemplaire de votre projet est sur la Forge. Pour en avoir un exemplaire pour travailler sur votre machine, il faut **cloner** le dépôt.

```mermaid
flowchart LR
    A[Forge] -- clonage --> B[Ordi]
```

Ensuite, pour travailler sur votre projet, vous allez **commiter (commit)** (XXX traduction fr commit ??? XXX) vos modifications, puis les **pousser (push)** sur le dépôt distant. Si vous travaillez de plusieurs endroits, ou bien à plusieurs sur le même projet, vous allez **tirer (pull)** les modifications des autres avant de pousser les vôtres.

```mermaid
flowchart LR
    A[Forge] -- tirer --> B[Ordi]
    B -- commit --> B
    B -- pousser --> A
```

Toutes ces étapes nécessitent une configuration minimale de git, que nous allons voir dans les sections suivantes.

## Clonage à sens unique d'un dépôt public

Si vous ne comptez pas pousser de modifications sur le dépôt distant, vous pouvez vous contenter de cloner le dépôt public. Aucune configuration n'est nécessaire dans ce cas, et cela vous permettra d'utiliser le projet en local, et de récupérer (`git pull`) les mises à jour du dépôt distant.

Exemple détaillé des opérations pour [cloner un dépôt public](./clone-public/index.md).

## Dépôt privé, authentification, synchronisation

Moyennant un tout petit peu plus de configuration, vous pourrez cloner des dépôts privés, pousser vos modifications vers un dépôt distant. Pour cela, le serveur avec lequel vous communiquez doit vous reconnaître, et vous devez lui donner les informations nécessaires pour vous authentifier.

Deux manières de s'authentifier sont possible :

-   en utilisant un couple **nom d'utilisateur / mot de passe**
-   en utilisant une **clé SSH**

Nous utiliserons la deuxième méthode, qui est potentiellement plus sécurisée, et plus pratique à l'usage.

Les différentes étapes :

-   préparer son authentification
-   cloner un dépôt pour lecture et écriture
-   envoyer des modifications sur un dépôt distant
-   récupérer les modifications du dépôt distant
-   résoudre des conflits
