# Avec un IDE

Exemple avec vscode

<figure>
<img src="../screenshots/usages/vscode/vscode-install-01.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-install-02.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-install-03.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-install-04.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-install-05.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-install-06.png"  class="img-fluid w-full">
<figcaption class="text-sm">Installation de vscode (avec chocolatey)</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-01.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-02.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-03.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-04.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-05.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-06.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-07.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-08.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-09.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-10.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-11.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-12.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-13.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-14.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-15.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-16.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/vscode/vscode-usage-17.png"  class="img-fluid w-full">
<figcaption class="text-sm">Utilisation git sous vscode</figcaption>
</figure>
