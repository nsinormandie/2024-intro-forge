# Authentification

Un des avantages de Git est qu'il permet de travailler en local sans avoir besoin d'une connexion internet. Cependant, pour pouvoir travailler en collaboration avec d'autres personnes, il est nécessaire de se connecter à un serveur distant pour pouvoir partager les modifications. Pour cela, il est nécessaire de s'authentifier.

Git peut communiquer avec un serveur distant avec deux protocoles :

-   **protocole HTTPS**, qui utilise un couple nom d'utilisateur/mot de passe ou jeton d'authentification
-   **protocole SSH**, qui utilise un couple clé privée/clé publique

Nous allons voir comment configurer Git pour utiliser le protocole SSH, qui est plus sécurisé et plus pratique à l'usage.

## Préparer ses clefs

### Vérifier l'existence d'une clé SSH

Première étape : vérifier si vous avez déjà une clé SSH. Pour cela, ouvrez un terminal (`Git Bash` sous windows, n'importe quel terminal sous macos et linux) et tapez :

```bash
cd # pour aller dans le répertoire personnel
ls -al .ssh/ # pour lister tous les fichiers du répertoire .ssh
```

-   si le répertoire .ssh n'existe pas : vous n'avez pas de clef SSH
-   si le répertoire .ssh existe, et contient des fichiers `id_xxxxxx` et `id_xxxxxx.pub` : vous avez déjà une clef SSH, vous pouvez sauter l'étape de génération de clef.

### Création d'une paire de clefs SSH

Dans le terminal, utilisez la commande `ssh-keygen` pour générer une nouvelle paire de clefs SSH :

```bash
ssh-keygen
```

-   appuyez sur `Entrée` pour accepter le chemin par défaut
-   appuyez sur `Entrée` pour ne pas mettre de mot de passe, ou entrez un mot de passe si vous le souhaitez[^1]

Un couple de clefs SSH est généré dans le répertoire `~/.ssh` par défaut. Selon le système et la version de SSH, il peut s'agir de clefs RSA, DSA, ou ED25519 [^2]. Vous pouvez refaire un

```bash
ls -al .ssh/
```

pour vérifier la présence des fichiers `id_xxxxxx` et `id_xxxxxx.pub`.

[^2]: Les clefs ED25519 sont celles recommandées aujourd'hui pour leur sécurité et leur rapidité.
[^1]: Un mot de passe sur la clef SSH est une sécurité supplémentaire, mais peut être un peu plus contraignant à l'usage. Sans mot de passe, la sécurité de la clef est alors celle du poste de travail.

<figure>
<img src="../screenshots/usages/clone/ssh-config-01.png"  class="img-fluid w-full">
<figcaption class="text-sm">Création du couple de clefs : publique <code>id_ed25519.pub</code> et privée <code>id_ed25519</code></figcaption>
</figure>

## Transmettre sa clef publique

Il faut maintenant donner la clef publique au serveur distant pour pouvoir utiliser notre clef privée pour s'authentifier.

-   naviguez vers votre vers les "Paramètres utilisateur" dans GitLab : `icône utilisateur / préférences / clés SSH`
-   copiez le contenu de la clef publique (fichier `id_xxxxx.pub` par défaut) - vous pouvez utiliser la commande `cat .ssh/id_xxxxx.pub` pour afficher le contenu de la clef et la copier.
-   cliquez sur "Ajouter une nouvelle clé"
-   collez le contenu dans le champ "Clé" et validez

???info "Les étapes en images"

    <figure>
    <img src="../screenshots/usages/clone/ssh-config-02.png"  class="img-fluid w-full">
    <figcaption class="text-sm">Barre latérale - icône utilisateur</figcaption>
    </figure>

    <figure>
    <img src="../screenshots/usages/clone/ssh-config-03.png"  class="img-fluid w-full">
    <figcaption class="text-sm">Préférences utilisateur</figcaption>
    </figure>

    <figure>
    <img src="../screenshots/usages/clone/ssh-config-04.png"  class="img-fluid w-full">
    <figcaption class="text-sm">Clés SSH</figcaption>
    </figure>

    <figure>
    <img src="../screenshots/usages/clone/ssh-config-05.png"  class="img-fluid w-full">
    <figcaption class="text-sm">Ajouter une nouvelle clé</figcaption>
    </figure>

    <figure>
    <img src="../screenshots/usages/clone/ssh-config-06.png"  class="img-fluid w-full">
    <figcaption class="text-sm">Afficher le contenu de la clef publique : `cat id_xxxxxx.pub`</figcaption>
    </figure>

    <figure>
    <img src="../screenshots/usages/clone/ssh-config-07.png"  class="img-fluid w-full">
    <figcaption class="text-sm">Sélectionner la clef, click droit</figcaption>
    </figure>

    <figure>
    <img src="../screenshots/usages/clone/ssh-config-08.png"  class="img-fluid w-full">
    <figcaption class="text-sm">Copier la clef</figcaption>
    </figure>

    <figure>
    <img src="../screenshots/usages/clone/ssh-config-09.png"  class="img-fluid w-full">
    <figcaption class="text-sm">Coller la clef dans le champ "Clé", vérifier/modifier les autres champs (la date d'expiration)</figcaption>
    </figure>

    <figure>
    <img src="../screenshots/usages/clone/ssh-config-10.png"  class="img-fluid w-full">
    <figcaption class="text-sm">La clef est enregistrée</figcaption>
    </figure>

## Configurer son identité locale

Lorsqu'on utilise git, les changements (commits) sont attribués à une personne. Pour cela, il est nécessaire de configurer son identité locale. Pour cela, utilisez les commandes suivantes :

```bash
git config --global user.name "Votre Nom"
git config --global user.email "votre.addresse@email.tld"
```

Si vous avez plusieurs identités (par exemple, une pour le travail et une pour les projets personnels), vous pouvez configurer une identité par projet en utilisant la commande `git config` sans l'option `--global`.

Dans le cas de la Forge, il est recommandé d'utiliser la même adresse email que celle utilisée pour votre compte (votre adresse académique) pour que les contributions soient bien attribuées à votre compte.

!!!danger "Attention"

    Toutes ces opérations ne sont à faire qu'une seule fois, pour associer un poste de travail et un serveur. Si vous travaillez avec plusieurs serveurs (ex: forge/gitlab/github..), ou bien avec plusieurs postes de travail, vous devrez répéter tout ou partie de ces opérations :

    - création d'une paire de clefs sur chaque nouveau poste de travail
    - association de la clef publique de chaque poste de travail avec chaque serveur
