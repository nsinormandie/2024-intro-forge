---
title: Créer un nouveau dépôt
---

# Créer un nouveau dépôt

Les étapes en images de la création d'un dépôt.

<figure>
<img src="../screenshots/usages/clone/clone-private-01.png"  class="img-fluid w-full">
<figcaption class="text-sm">Bouton "Nouveau projet" dans "Projets"</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/clone-private-02.png"  class="img-fluid w-full">
<figcaption class="text-sm">Créer un projet vide</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/clone-private-03.png"  class="img-fluid w-full">
<figcaption class="text-sm">Remplir les informations pour le nouveau projet (nom, ...)</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/clone-private-04.png"  class="img-fluid w-full">
<figcaption class="text-sm">Choisir le groupe dans lequel le dossier sera créé (utilisateur pour l'instant)</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/clone-private-05.png"  class="img-fluid w-full">
<figcaption class="text-sm">Configurer la visibilité (public/privé/interne), création avec le README, et clic "Créer le projet"</figcaption>
</figure>

<figure>
<img src="../screenshots/usages/clone/clone-private-06.png"  class="img-fluid w-full">
<figcaption class="text-sm">Le projet a été crée</figcaption>
</figure>

Le dépôt est créé, il est vide pour l'instant. Vous pouvez l'éditer :

-   en ligne en utilisant l'Ide web de GitLab, c'est pratique pour un **petit changement rapide** sur une autre machine que la sienne, mais **pas recommandé** pour des modifications plus conséquentes.
-   en local, en clonant le dépôt, c'est plus **confortable** pour des modifications plus conséquentes. **A privilégier tout le temps sauf quand on n'est pas sur sa machine.**
