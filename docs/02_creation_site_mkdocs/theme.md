# Customiser le thème

## La palette de couleur

La palette de couleur est intégrée par défaut au thème Pyodide-Mkdocs-Theme mais les fonctionnalités de [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/#changing-the-colors) restent disponibles. 

Pour changer la palette de couleurs, il faut modifier le fichier `mkdocs.yml` : les réglages prennent alors le pas sur les valeurs par défaut.

### La propriété `primary`

!!! info "`primary`"
	La couleur désignée par la propriété `primary` est utilisée pour l'en-tête, la barre latérale, les liens texte et plusieurs autres composants. La valeur par défaut est `indigo`.
 
!!! note "Couleurs possibles"
	`red` `pink` `purple` `deep purple` `indigo` `blue` `light blue` `cyan` `teal` `green` `light green` `lime` `yellow` `amber` `orange` `deep orange` `brown` `grey` `blue grey` `black` `white` 
	
!!! abstract "Le code" 
	Préciser la valeur de `primary` dans le fichier `mkdocs.yml`. Par exemple : 

	```yaml
	theme:
	  palette:
		primary: deep orange
	```
 
### La propriété `accent`

!!! info "`accent`"
	La couleur désignée par la propriété `accent` est utilisée pour les éléments avec lesquels il est possible d'interagir, par exemple les liens, boutons et barres de défilement survolés.    
	La valeur par défaut est `indigo`.

!!! note "Couleurs possibles"
	`red` `pink` `purple` `deep purple` `indigo` `blue` `light blue` `cyan` `teal` `green` `light green` `lime` `yellow` `amber` `orange` `deep orange`

 
!!! abstract "Le code" 
	Préciser la valeur de `accent` dans le fichier `mkdocs.yml`. Par exemple :

	```yaml
	theme:
	  palette:
		accent: blue
	```

Il est possible de tester les diférentes combinaisons de couleurs sur le site [Material for Mkdocs](https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/#primary-color).
 
### Jour ou Nuit

!!! info "mode jour/nuit"

	[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/#changing-the-colors) propose une propriété `scheme` avec deux modes, __light mode__, qui est désigné par 
	 `default`, et  __dark mode__, qui est désigné par `slate`. 
 
!!! abstract "Le code" 
	
	Préciser la valeur de `scheme` dans le fichier `mkdocs.yml`. Par exemple :

	```yaml
	theme:
	  palette:
		scheme: default
	```


### Choisir les couleurs

Proposer une palette de couleurs claires et sombres rend votre documentation agréable à consulter à différents moments de la journée, afin que l'utilisateur puisse choisir en conséquence.

Pour cela, pour chaque mode on défnit les couleurs et les caractéristiques du bouton de bascule des deux modes.

!!! info "`toggle`"

	La propriété `toggle` permet de placer un bouton de bascule entre les deux modes : on peut choisir la forme du bouton et le message au survol de la souris.


!!! note "Choix des icônes"
	Voici les quelques combinaisons valides pour les deux propriétés `icon` :

	* :material-brightness-7: + :material-brightness-4: – `material/brightness-7` + `material/brightness-4`
	* :material-toggle-switch: + :material-toggle-switch-off-outline: – `material/toggle-switch` + `material/toggle-switch-off-outline`
	* :material-weather-night: + :material-weather-sunny: – `material/weather-night` + `material/weather-sunny`
	* :material-eye: + :material-eye-outline: – `material/eye` + `material/eye-outline`
	* :material-lightbulb: + :material-lightbulb-outline: – `material/lightbulb` + `material/lightbulb-outline`


!!! abstract "Le code" 
	Préciser dans le fichier `mkdocs.yml` les valeurs de `icon` (forme du bouton) et de `name` (message au survol) du bouton de bascule dans chacun des deux modes :  `default` (clair) et `slate` (sombre). Par exemple :
	```yaml
	theme:
	  palette: 
		# Palette toggle for light mode
		- scheme: default
		  primary: deep orange
		  accent: blue
		  toggle:
			icon: material/weather-sunny
			name: Passer au mode nuit
		# Palette toggle for dark mode
		- scheme: slate
		  primary: deep orange
		  accent: blue
		  toggle:
			icon: material/weather-night
			name: Passer au mode jour
	```



### Préférence utilisateur

!!! info "Propriété `media`"

	Chaque palette de couleurs peut être liée aux préférences système de l'utilisateur en matière de lumière et apparence sombre en utilisant une requête média.  


!!! abstract "Le code" 
	Il faut ensuite ajouter le code suivant dans le fichier `mkdocs.yml`
	
	```yaml
	theme:
	  palette:

		# Palette toggle for light mode
		- media: "(prefers-color-scheme: light)"
		  scheme: default
		  toggle:
			icon: material/brightness-7
			name: Switch to dark mode

		# Palette toggle for dark mode
		- media: "(prefers-color-scheme: dark)"
		  scheme: slate
		  toggle:
			icon: material/brightness-4
			name: Switch to light mode
	```
 
## Ajouter son style

Si vous souhaitez modifier certaines couleurs ou modifier l'espacement de certains éléments,
vous pouvez le faire dans une feuille de style séparée. Le moyen le plus simple est de créer un
nouveau fichier `extra.css` dans un répertoire `stylesheets` à placer dans le répertoire `docs` 

```{ .sh .no-copy }
.
├─ docs/
│  └─ stylesheets/
│     └─ extra.css
└─ mkdocs.yml
```

!!! abstract "Le code"
	Il faut ensuite ajouter le code suivant dans le fichier `mkdocs.yml`:

	```yaml
	extra_css:
	  - stylesheets/extra.css
	```
 
!!! example "Exemple de fichier `extra.css`"
	
	```css
	.md-typeset h1 {
    color: purple;
    }
	
	.md-typeset h2{
		color: purple; 
	}
	
	.md-typeset h3{
		color: rgb(120, 90, 140); 
	}
	```
	
## Modifier le logo du site

Un logo "stairs-up" apparaît par défaut avec le thème installé. Il est possible de choisir un autre logo dans la bibliothèque : [Logos Material Design Icons](https://pictogrammers.com/library/mdi/){ target="_blank" }

!!! abstract "Le code"
	Préciser la valeur de `logo` dans le fichier `mkdocs.yml`. 

	```yaml
	theme:
      icon:
        logo: material/stairs-up  # remplacer stairs-up par l'icone choisie
    ```

## Tout savoir sur la configuration du site

On peut trouver dans la documentation complète relative à la [configuration d'un site créé avec material pour mkdocs](https://squidfunk.github.io/mkdocs-material/setup/) d'autres paramètres à personnaliser.
