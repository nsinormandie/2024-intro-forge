

# Accéder au service

La forge fait partie du portail [apps.education.fr](https://portail.apps.education.fr/signin).
Apps étant relié à l'annuaire fédéré des enseignants et des agents, tout personnel du ministère de l'Éducation nationale peut disposer d'un compte sur la forge via son compte Apps.

L'accès au portail [apps.education.fr](https://portail.apps.education.fr/signin), se fait donc via authentification par son académie.

![authentification apps](images/authentification_apps.png){width=50% .screen .center}

![choix académie](images/choix_academie.png){width=50% .screen .center}

L'onglet "Mon espace" ne contenant que les applications que l'on a mises en favori, il faut chercher la vignette "Forge des communs numériques éducatifs" dans l'onglet "Les services".
Un clic sur `+` permet d'ajouter la vignette à l'onglet "mon espace". 
![vignette Forge](images/vignette_Forge.png){width=50% .screen .center}
