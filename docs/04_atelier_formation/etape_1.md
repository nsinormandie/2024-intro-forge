---
title: Étape 1
hide:
    - toc
---

# Utiliser l'IDE python et les QCM

Voici un exercice adapté d'un exercice créé par Sébastien Hoarau et Nicolas Revéret sur le site [Codex](https://codex.forge.apps.education.fr/exercices/couleurs/).


!!! danger "Consigne"
    Réaliser l'exercice (questions 1 et 2), "comme un élève", avec pour objectif d'observer le fonctionnement de l'IDE : 

    - lorsqu'on entre un code erroné ; 
    - lorsqu'on active/désactive les tests publics en cliquant sur `###` ;
    - lorsqu'on atteint le nombre maximum d'essais ; 
    - lorsqu'on entre un code correct.

    Puis répondre à la question 3, en allant si besoin explorer sur la forge les fichiers qui ont permis de réaliser cette page.

!!! quote ""
    ###  Couleurs en HTML

    Une couleur en HTML est représentée par une chaine de caractères de sept caractères dont le premier est `'#'` ; les six autres, groupés 2 par 2 forment 3 entiers en hexadécimal (base 16). Le premier entier est la quantité de rouge ; le deuxième la quantité de vert et le troisième est la quantité de bleu. Ces trois valeurs hexadécimales sont comprises entre `#!py "00"` et `#!py "FF"`.

    Voici quelques couleurs en HTML : 
        
    - `#!py "#ED774F"` : <span style="color:#ED774F;"> deep orange</span>,
    - `#!py "#0000FF"` : <span style="color:#0000FF">bleu</span>, 
    - `#!py "#000000"` :  noir.

    Une autre façon de représenter une couleur est par un triplet $(r, v, b)$ de valeurs décimales comprises entre $0$ et $255$ : $r$ est la quantité de rouge, $v$ la quantité de vert et $b$ la quantité de bleu.

    On souhaite écrire une fonction `html_vers_rvb` qui prend une chaine de caractères représentant une couleur HTML en paramètre et qui renvoie le triplet de décimaux $(r, v, b)$ représentant la même couleur.

    On rappelle les valeurs décimales des 16 _chiffres_ hexadécimaux sont : `"0"` vaut $0$, jusqu'à `"9"` qui vaut $9$, puis `"A"` vaut $10$, `"B"` vaut $11$ ainsi de suite jusqu'à `"F"` qui vaut $15$.

    Pour calculer la valeur décimale d'un nombre hexadécimal de deux chiffres $(ab)_{16}$, on fera la somme du produit de la valeur décimale du chiffre des seizaines $a$ par $16$ et de la valeur décimale du chiffre des unités $b$.

    ??? tip "Indications"

        - Le nombre hexadécimal `#!py "B5"` vaut $11\times 16 + 5$ soit $181$ en décimal.

        - `#!py "00"` vaut $0\times 16 + 0$ soit $0$.

        - Si `#!py couleur = "#F307D6"`, `#!py couleur[1]` vaut `#!py 'F'`.

??? question "Question 1"

    Écrire la fonction `hex_int` qui prend deux chaines d'un caractère `a` et `b` en paramètres (de sorte que $(ab)_{16}$ est un entier en hexadécimal) et renvoie la valeur décimale associée, en utilisant le dictionnaire `HEX_DEC` ; 

    Exemples :
    !!! quote ""
        ```pycon
        >>> hex_int('B', '5')
        181
        >>> hex_int('0', '0')
        0
        ```

    {{ IDE('couleurs/exo', SANS='int') }}

??? question "Question 2"
    Écrire la fonction `html_vers_rvb` en appelant de la fonction précédente, même si celle-ci n'a pas été trouvée.

    Exemples
    !!! quote ""
        ```pycon
        >>> html_vers_rvb("#ED774F")
        (237, 119, 79)
        >>> html_vers_rvb("#0000FF")
        (0, 0, 255)
        >>> html_vers_rvb("#000000")
        (0, 0, 0)
        ```

    {{ IDE('couleurs/exo_b') }}

{{ multi_qcm(
    [
    "Peut-on utiliser la fonction `hex_int` de la question 1 pour répondre à la question 2, même si on ne l'a pas trouvée ?",
    ["non", "oui", "peut-être"],
    [2]
    ],
    [
    "Peut-on faire apparaître le corrigé d'une question , même si on ne l'a pas résolue ?",
    ["non", "oui", "peut-être"],
    [2]
    ],
    [
    "Peut-on désactiver les tests publics ? ",
    ["non", "oui", "peut-être"],
    [2]
    ],
    [
    "Peut-on désactiver les tests privés ? ",
    ["non", "oui", "peut-être"],
    [1]
    ],
    [
    "Peut-on utiliser la fonction `int` dans la question 1, par exemple pour trouver la valeur décimale associée à un chiffre hexadécimal ? ",
    ["non", "oui", "peut-être"],
    [1]
    ],
    [
    "Le fichier python utilisé dans la question 1 comporte-t-il une section `secrets` ?",
    ["non", "oui", "peut-être"],
    [2]
    ],
    [
    "Le fichier python utilisé dans la question 2 comporte-t-il une section `env` ?",
    ["non", "oui", "peut-être"],
    [2]
    ],
    qcm_title = "Question 3",
    admo_class = 'question',
    admo_kind = '???',
    multi = False,
    DEBUG = False
    ) 
}} 