# Présentation de forge.apps.education.fr

Ce site est le document d'accompagnement de la formation "Forge des Communs Numériques Éducatifs" proposée par le PCD NSI Normandie (juillet 2024).



Adresse de ce site : [nsinormandie.forge.apps.education.fr/2024-intro-forge/](https://nsinormandie.forge.apps.education.fr/2024-intro-forge/)
