---
author: Mireille Coilhac
title: 👏 Crédits
---

Le site est hébergé par  [la forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/){:target="_blank" }.

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/), et surtout [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/) pour la partie Python.

😀 Un grand merci à Frédéric Zinelli, et Vincent-Xavier Jumel qui ont réalisé la partie technique de ce site. 

Le logo :material-magic-staff: fait partie de MkDocs sous la référence `magic-staff`  [https://pictogrammers.com/library/mdi/](https://pictogrammers.com/library/mdi/){:target="_blank" }

